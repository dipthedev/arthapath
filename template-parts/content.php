<div class="col-md-8">
    <div class="pt-0 p-3" style="background: white;">
        <article class="post-entry mt-5">
            <?php get_template_part('template-parts/single-heading');?>
            <div class=" pt-3">
                <?php the_post_thumbnail('full');?>
            </div>
            <div class="pt-3">   <?php the_content();?></div> <?php if(has_tag())
{echo '<span class="blog-tags minor-meta">';
	the_tags('<strong>'.__('Tags :','avia_framework').'</strong><span> ', ' ', ' ');
	echo '</span></span>';}?>   
</article>

<div class="pt-3">
<?php dynamic_sidebar('belowcommemt');?>
</div>
    </div>
    <hr>
    <div class="mt-3" style="background: white;">
        <?php comments_template();?>
    </div>
    <div class="mt-3 p-2" style="background: white;">
        <?php get_template_part('related');?>
    </div>
</div>
<div class="col-md-4 mt-5">
<?Php get_template_part('sidebar');?><!-- <div class="margin-box"></div> -->
</div>
<!-- .col-md-8 -->


