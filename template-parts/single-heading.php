<header>
    <h1 class="single-heading text-center pt-5"><?php the_title();?></h1>
    <hr><div class="row text-muted post-meta">
        <div class="col-md-5 d-flex align-items-center">
            <span class="blog-title d-flex">
                <div class="author-img ">
                    <div class="pt-3">
                        <?php
                        $author_id = get_the_author_meta('ID');
                        $author_image = get_field('image', 'user_'. $author_id );?>
                        <div class="author d-flex">
                            <div class="author-image">
                                <?php if($author_image):?>
                                <img src="<?php echo $author_image['sizes']['thumbnail'];?>">
                                <?php else:?>
                                <i class="fa fa-user"></i>
                                <?php endif;?> </div>
                                <h5 style="font-size:14px;">
                                <div class="name pt-1 ml-3">
                                    <a href="/?author=<?php echo $author_id; ?>"
                                        style="text-transform:uppercase;color: #7b519d;">
                                        <?php the_author(); ?>
                                    </a>
                                    <div class="pt-1">
                                        प्रकाशित मिति :
                                        <?php the_time('j F, Y g:i a ' ); ?>
                                    </div>
                                </div>
                            </h5>
                        </div>
                    </div>
                </div>
              </span> </div>
            <div class="col-md-7 d-flex justify-content-end pt-5">
            <!-- Go to www.addthis.com/dashboard to customize your tools -->
            <div class="addthis_inline_share_toolbox"></div>
        </div>
    </div>
</header>
<hr>