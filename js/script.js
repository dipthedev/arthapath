$(function () {
    var videos = $(".post-entry p>iframe").addClass("embed-responsive-item");
    videos.parent().addClass("embed-responsive embed-responsive-16by9");
    jQuery('.nav-link').click( function(e) {
        jQuery('.collapse').collapse('hide');
    });

    $('.video').owlCarousel({
        items: 1,
        merge: true,
        loop: true,
        margin: 10,
        video: true,
        lazyLoad: true,
        center: true,
        responsive: {
            480: {
                items: 2
            },
            600: {
                items: 4
            }
        }
    })
});