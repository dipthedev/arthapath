<?php
if ( function_exists( 'add_theme_support' ) ) { 
	add_theme_support( 'post-thumbnails' );
	add_action( 'after_setup_theme', 'create_thumb' );
	function create_thumb() {
		add_image_size( 'thumbnail', 150, 150, true );
		add_image_size( 'medium', 400, 220, true );
		add_image_size( 'large', 768, 768);
	}
}
add_theme_support( 'html5', array(
    'search-form',
    'gallery',
    'caption',
) );
add_theme_support('post-formats', array('video','gallery'));
add_theme_support( 'jetpack-responsive-videos' );
if ( ! isset( $content_width ) ) {
	$content_width = 720;
}
//menu
function register_my_menus() {
	register_nav_menus(
	  array(
		'main-menu' => __( 'Main Menu Header' ),
		'sub-menu' => __( 'Sub Menu' )
	  )
	);
  }
add_action( 'init', 'register_my_menus' );
function wp_get_menu_array($current_menu) {
 
	$array_menu = wp_get_nav_menu_items($current_menu);
	$menu = array();
	foreach ($array_menu as $m) {
		if (empty($m->menu_item_parent)) {
			$menu[$m->ID] = array();
			$menu[$m->ID]['ID']      =   $m->ID;
			$menu[$m->ID]['title']       =   $m->title;
			$menu[$m->ID]['url']         =   $m->url;
			$menu[$m->ID]['children']    =   array();
		}
	}
	$submenu = array();
	foreach ($array_menu as $m) {
		if ($m->menu_item_parent) {
			$submenu[$m->ID] = array();
			$submenu[$m->ID]['ID']       =   $m->ID;
			$submenu[$m->ID]['title']    =   $m->title;
			$submenu[$m->ID]['url']  =   $m->url;
			$menu[$m->menu_item_parent]['children'][$m->ID] = $submenu[$m->ID];
		}
	}
	return $menu;
	 
  }


//registering widgets
function appharu_widgets_init() {
	$widgets = array(
		array('id' => 'logoadd1','name'=>'logo left add'),
		array('id' => 'logoadd2','name'=>'logo right add'),
		array('id' => 'banneeradd-1','name'=>'ब्यानरtop'),
		array('id' => 'banneeradd-2','name'=>'ब्यानर-below'),
		array('id' => 'banneeradd-3','name'=>'अर्थपथ विशेष-tala'),
		array('id' => 'banneeradd-4','name'=>'बैंकिङ tala'),
		array('id' => 'banneeradd-5','name'=>'कर्णाली tala'),
		array('id' => 'banneeradd-6','name'=>'फोटोफिचरमाथि'),
		array('id' => 'banneeradd-7','name'=>'फोटोफिचर bottom'),
		array('id' => 'sidebaradd-1','name'=>'बैंकिङ sidebar'),
		
		
	);
	foreach ($widgets as $widget) {
		register_sidebar( array(
			'name'          => esc_html__( $widget['name'], 'appharu' ),
			'id'            => $widget['id'],
			'description'   => esc_html__( 'Add widgets here.', 'appharu' ),
			'before_widget' => '<section class="widget">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		) );
	}

}
add_action( 'widgets_init', 'appharu_widgets_init' );
//enquee css/scripts
function appharu_scripts() {
	wp_enqueue_style( 'bootstrap-style', get_stylesheet_directory_uri().'/css/bootstrap.min.css' );
	wp_enqueue_style( 'base-style', get_stylesheet_directory_uri().'/css/base.css' );
	wp_enqueue_style( 'google-font', 'https://fonts.googleapis.com/css?family=Mukta:400,500,700&amp;subset=devanagari' );
	wp_enqueue_style( 'owl-style', get_stylesheet_directory_uri().'/owl/assets/owl.carousel.min.css',array(),'0.2' );
	wp_enqueue_style( 'appharu-style', get_stylesheet_uri(),array(),'2.56' );
	
	wp_enqueue_script( 'jquery-minified', 'https://code.jquery.com/jquery-3.3.1.min.js', array(), '4.0.0.beta', true );
	wp_enqueue_script( 'font-awesome', 'https://use.fontawesome.com/releases/v5.0.8/js/all.js', array(), '', false );
	wp_enqueue_script( 'popper-js', get_stylesheet_directory_uri() . '/js/popper.min.js', array(), '1.11.0', true );
	wp_enqueue_script( 'bootstrap-js', get_stylesheet_directory_uri() . '/js/bootstrap.min.js', array(), '4.0.0.beta', true );
	wp_enqueue_script( 'owl-js', get_stylesheet_directory_uri() . '/owl/owl.carousel.min.js', array(), '0.2', true );
	wp_enqueue_script( 'custom-js', get_stylesheet_directory_uri() . '/js/script.js', array(), '0.1', true );
}
add_action( 'wp_enqueue_scripts', 'appharu_scripts' );

//Lets add Open Graph Meta Info

function insert_fb_in_head() {
	global $post;
	if ( !is_singular()) //if it is not a post or a page
		return;
        echo '<meta property="fb:admins" content="changingmission"/>';
		echo '<meta property="fb:app_id" content="1774296949456204">';
        echo '<meta property="og:title" content="' . get_the_title() . '"/>';
        echo '<meta property="og:type" content="article"/>';
        echo '<meta property="og:description" content="' . get_the_excerpt() . '"/>';
        echo '<meta property="og:url" content="' . get_permalink() . '"/>';
        echo '<meta property="og:site_name" content="'. get_bloginfo('name') .'"/>';
	if(!has_post_thumbnail( $post->ID )) {
		$default_image= get_stylesheet_directory_uri()."/img/logo.png";
		echo '<meta property="og:image" content="' . $default_image . '"/>';
	}
	else{
		$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
		echo '<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>';
	}
	echo "";
}
add_action( 'wp_head', 'insert_fb_in_head', 5 );

require get_template_directory().'/paging.php';