 <?php $args = array('showposts' => 1, 'cat' => ''); $loop = new WP_Query( $args ); while($loop->have_posts()): $loop->the_post(); ?>
        <div class="full-background">
        <a href="<?php the_permalink();?>">
			 <?php if( get_field('special_title') ): ?>
                    <h3 class="special-title text-center" style="color: #13a64f;font-size: 22px;font-weight: 300;">
                        <span class="special-title"><?php the_field('special_title'); ?></span>
                    </h3>
                    <?php endif; ?>
            <h1 class="display-4 banner-title text-center pt-5 mbl-font ">
                <?php the_title()?>
            </h1>
            </a>
            <div class="pt-0">
            <?php if( get_field('title') ): ?>
                    <h3 class="sub-title text-center" style="color: #13a64f;font-size: 22px;font-weight: 300;">
                        <?php the_field('title'); ?>
                    </h3>
                    <?php endif; ?>
                    <a href="<?php the_permalink();?>"> <?php the_post_thumbnail('full')?> </a>
            <p class="lead p-3">
                <?php echo wp_trim_words(get_the_excerpt(),50,'');?>
            </p>
            <hr class="my-4">
            <p class="lead text-center">
                <button class="btn btn-primary btn-lg rounded-0 read-more" role="button">
                <a href="<?php the_permalink();?>">  <span class="more-text">  पुरा पढ्नुहोस् </span> </a>
                </button>
            </p>
        </div>
        </div>
        <?php endwhile; wp_reset_postdata();?>
   
