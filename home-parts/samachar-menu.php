<nav class="navbar navbar-expand-lg navbar-dark col p-0">
    <a class="navbar-brand" href="#">समाचार</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false"
        aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="main-menu">
        <ul class="navbar-nav mr-auto">
        </ul>
        <ul class="navbar-nav jstify-content-end">
            <a href="">
                <span class="more-page">थप समाचार</span>
            </a>
        </ul>
    </div>
</nav>