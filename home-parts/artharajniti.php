<div class="col-md-12">
    <div class="cat-title">अर्थ राजनीति</div>
</div>
<?php $args = array('showposts' => 3, 'cat' => '10'); $loop = new WP_Query( $args ); while($loop->have_posts()): $loop->the_post(); ?>
<div class="col-md-4 main-list clearfix">
    <div style="position:relative">
        <a href="<?php the_permalink();?>">
            <?php the_post_thumbnail('medium');?>
        </a>
    </div>
    <h5 class="pt-2">
        <a href="<?php the_permalink();?>">
            <?php the_title();?>
        </a>
    </h5>
</div>
<?php endwhile; wp_reset_postdata();?>