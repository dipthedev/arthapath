<div class="cat-title">ताजा</div>
<div class="pt-3">
            <?php $i=0; $args = array('showposts' => 3, 'cat' => ''); $loop = new WP_Query( $args ); while($loop->have_posts()): $loop->the_post(); if($i++<1){?>
            <div class="width-full">
                <?php the_post_thumbnail('large');?>
            </div>
            <h4 class="pt-3">
                <?php the_title()?>
            </h4>
            <p>
                <?php echo wp_trim_words(get_the_excerpt(),20,'');?>
            </p>
        
        <div class=" small-list-img custom-list">
            <?php } else{ ?>
            <div class="mb-2">
                <a href="<?php the_permalink();?>">
                    <div class="media">
                        <?php the_post_thumbnail('thumbnail');?>
                        <div class="media-body">
                            <h5 class="mt-0 ml-2">
                                <?php echo wp_trim_words(get_the_title(),7,'');?>
                            </h5>
                        </div>
                    </div>
                </a>
            </div>
            <hr>
            <?php } endwhile; wp_reset_postdata();?>
        </div>
    </div>
