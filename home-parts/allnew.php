<?php $args = array('showposts' => 3, 'cat' => ''); $loop = new WP_Query( $args ); while($loop->have_posts()): $loop->the_post(); ?>
<div class="col-md-4">
    <div class="cat-title">समाचार</div>
    <div style="position:relative">
        <a href="<?php the_permalink();?>">
            <?php the_post_thumbnail('medium');?>
        </a>
        <h5 class="banner-h5">
            <a href="<?php the_permalink();?>">
                <?php the_title();?>
            </a>
        </h5>
    </div>

</div>
<?php endwhile; wp_reset_postdata();?>
<!-- <div class="col-md-3">
    <img class="w-100 mt-5" src="<?php echo get_stylesheet_directory_uri();?>/img/three.gif" alt="Logo">
</div> -->