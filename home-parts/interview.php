<div class="col-md-12">
    <div class="cat-title">अन्तर्वार्ता</div>
</div>
<div class="row no-gutters pb-3">
    <div class="col-md-8 ">
        <?php $i=0; $args = array('showposts' =>3, 'cat' => '21'); $loop = new WP_Query( $args ); while($loop->have_posts()): $loop->the_post(); if($i++<1){?>
        <a href="<?php the_permalink();?>">
            <div class="entertains mt-3">
                <div class="back-img-big main-imgs mbl-img"
                    style="background: url(<?php echo get_the_post_thumbnail_url( $post->ID,'large' ); ?>)center 0px / cover no-repeat;height:500px">
                </div>
                <div class="overlay-enter"></div>
                <div class="all">
                    <h1 class="enter-bigtxt" style="font-size: 25px;">
                        <?php the_title();?>
                    </h1>
                    <p class="text-left text-light mbl-pp  excerpt-small">
                        <?php echo wp_trim_words(get_the_excerpt(),40,'');?>
                    </p>
                </div>
            </div>
        </a>
    </div>
    <div class="col-md-4 mt-3">
        <?php } else{?>
        <a href="<?php the_permalink();?>">
            <div class=" enter-div ">
                <div class="back-img-small main-imgs"
                    style="background: url(<?php echo get_the_post_thumbnail_url( $post->ID,'large' ); ?>)center 0px / cover no-repeat ;height:250px">
                </div>
                <div class="overlay-enter" style="border-right:0px;"></div>
                <h1 class=" enter-txt" style="font-size:22px;">
                    <?php echo wp_trim_words( get_the_title(), 10 ); ?>
                </h1>
            </div>
        </a>

        <?php } endwhile; wp_reset_postdata();?>

    </div>
</div>
<style>
    .back-img-big {
    height: 400px;
}
.entertains {
    position: relative;
}
.back-img-small {
    height: 200px;
}
h1.enter-txt {
    position: absolute;
    bottom: -8px;
    padding: 12px;
    color: #dbdadf;
    text-shadow: 0px 4px 0px black;
}
h1.enter-bigtxt {
    line-height: 1.4;
    color: #dbdadf;
    font-size: 38px;
    transition: 1s;
}
.enter-div {
    position: relative;
}
    h1.enter-bigtxt {
    line-height: 1.4;
    color: #dbdadf;
    font-size: 38px;
    transition: 1s;
}
    .overlay-enter {
    border-right: 5px solid;
    border-bottom: solid 5px;
    position: absolute;
    bottom: 0px;
    background: linear-gradient(45deg, #000000, #00000021);
    padding: 19px;
    width: 100%;
    height: 100%;
    color: white !important;
    font-size: 43px;
}
.all {
    position: absolute;
    bottom: 0px;
    padding: 19px;
    width: 100%;
    color: white !important;
    font-size: 43px;
}
.entertains {
    position: relative;
}

h1.enter-txt {
    position: absolute;
    bottom: -8px;
    padding: 12px;
    color: #dbdadf;
    text-shadow: 0px 4px 0px black;
}
p.excerpt-small {
    font-size: 14px !important;
}
</style>