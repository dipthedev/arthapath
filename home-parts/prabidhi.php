<div class="cat-title">प्रविधि</div>
<?php $i=0; $args = array('showposts' => 4, 'cat' => '12'); $loop = new WP_Query( $args ); while($loop->have_posts()): $loop->the_post();if($i++<1){?>
    <div class="mt-3 international-img">
        <a href="<?php the_permalink();?>">
           <div class="back-img"
                style="background: url(<?php echo get_the_post_thumbnail_url( $post->ID,'full' ); ?>)center 0px / cover no-repeat;height:220px;">
            </div></a>
            <div class="overlay-information"></div>
           <h5 class="pt-3 international-text text-light" style="font-size: 19px;">
                 <a href="<?php the_permalink();?>"  class="text-light"> <?php the_title();?></a>
				</h5>
</div>
<hr>
<?php } else{ ?>
    <div class="media loksewa-list pt-3 pb-3">
             <a href="<?php the_permalink();?>">
             <div class="mr-3 sr-thumb-t">
                     <?php the_post_thumbnail('thumbnail');?>
                 </div>
             </a>
             <div class="media-body">
            
                     <h5 class="mt-0" style="font-weight: 600; font-size: 17px;">
                          <a href="<?php the_permalink();?>"> <?php the_title();?> </a>
                     </h5>
                  </div>
         </div> 
<?php } endwhile; wp_reset_postdata();?>