<div class="cat-title">बिमा</div>
<?php $i=0; $args = array('showposts' => 4, 'cat' => '7'); $loop = new WP_Query( $args ); while($loop->have_posts()): $loop->the_post();if($i++<1){?>
    <div class="mt-3 international-img">
        <a href="<?php the_permalink();?>">
           <div class="back-img"
                style="background: url(<?php echo get_the_post_thumbnail_url( $post->ID,'full' ); ?>)center 0px / cover no-repeat;height:336px;">
            </div></a>
            <div class="overlay-information"></div>
           <h5 class="pt-3 international-text text-light" style="font-size:25px;">
                 <a href="<?php the_permalink();?>"  class="text-light"> <?php the_title();?></a>
				</h5>
</div>
<?php } else{ ?>
    <div class="media loksewa-list pt-3 pb-3">
             <a href="<?php the_permalink();?>">
             <div class="mr-3 sr-thumb-t">
                     <?php the_post_thumbnail('thumbnail');?>
                 </div>
             </a>
             <div class="media-body">
            
                     <h5 class="mt-0" style="font-weight: 600; font-size: 20px;">
                          <a href="<?php the_permalink();?>"> <?php the_title();?> </a>
                     </h5>
                  </div>
         </div> 
<?php } endwhile; wp_reset_postdata();?>
<style>
h5.international-text {
    position: absolute;
    font-size: 19px;
    bottom: -11px;
    padding: 12px;
    background: #1e1e1e26;
    color: #fff;
}
.international-img {
    position: relative;
}
.full-background {
    background: #fff;
}
.sr-thumb-t img {
    width: 80px;
    height: 70px;
}
</style>