<div class="row">
    <div class="col-md-6">
        <?php $args = array('showposts' => 1, 'cat' => ''); $loop = new WP_Query( $args ); while($loop->have_posts()): $loop->the_post(); ?>
        <div class="">
            <figure>
                <?php the_post_thumbnail('full');?>
            </figure>
            <h5>
                <a href="<?php the_permalink();?>">
                    <?php the_title();?>
                </a>
            </h5>
            <div class="pt-1">
                <p>
                    <?php echo wp_trim_words(get_the_excerpt(),45,'');?>
                </p>
            </div>
        </div>
        <?php endwhile; wp_reset_postdata();?>
    </div>
    <div class="col-md-6">
        <div class="row">
            <?php $args = array('showposts' => 4, 'cat' => ''); $loop = new WP_Query( $args ); while($loop->have_posts()): $loop->the_post(); ?>
            <div class="col-md-6">
                <figure>
                    <?php the_post_thumbnail('full');?>
                </figure>
                <h6>
                    <a href="<?php the_permalink();?>">
                        <?php echo wp_trim_words(get_the_title(),10,'');?>
                    </a>
                </h6>
            </div>
            <?php endwhile; wp_reset_postdata();?>
        </div>
    </div>
</div>