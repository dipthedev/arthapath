<div class="cat-title">पूर्वाधार
</div>
<div class="row">
<?php $args = array('showposts' => 6, 'cat' => '11'); $loop = new WP_Query( $args ); while($loop->have_posts()): $loop->the_post(); ?>
        <div class="col-md-4 mt-3">
        <div class="boxi">
     <a href="<?php the_permalink();?>"> <?php the_post_thumbnail('large')?> </a>
             <h5 class="small-font pt-3" style="font-size:20px;color:black">
             <?php echo wp_trim_words( get_the_title(), 10 ); ?>
                    </h5> 
        </div>
        </div>
        <?php endwhile; wp_reset_postdata();?>
   </div>
   <style>
   .boxi{
    box-shadow: 0 0 4px 0;
    color: #e4e7f0;
    padding: 12px;
    height: 100%;
   }
   </style>
