<div class="cat-title">अर्थ राजनीति</div>
<div class="row">
        <div class="col-md-6">
            <?php $i=0; $args = array('showposts' => 6, 'cat' => '10'); $loop = new WP_Query( $args ); while($loop->have_posts()): $loop->the_post(); if($i++<1){?>
            <figure>
                <?php the_post_thumbnail('large');?>
            </figure>
            <h4>
                <?php the_title()?>
            </h4>
            <p style="font-size: 19px;">
                <?php echo wp_trim_words(get_the_excerpt(),50,'');?>
            </p>
        </div>
        <div class="col-md-6 small-list-img custom-list">
            <?php } else{ ?>
            <div class="pt-0">
                <a href="<?php the_permalink();?>">
                    <div class="media">
                        <?php the_post_thumbnail('thumbnail');?>
                        <div class="media-body">
                            <h5 class="mt-0 ml-2">
                                <?php echo wp_trim_words(get_the_title(),7,'');?>
                            </h5>
                        </div>
                    </div>
                </a>
            </div>
            <hr>
            <?php } endwhile; wp_reset_postdata();?>
        </div>
    </div>