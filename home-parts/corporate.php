<div class="cat-title">कर्पोरेट</div>
<div class="wrapper">
<?php $i=0; $args = array('showposts' => 4, 'cat' => '22'); $loop = new WP_Query( $args ); while($loop->have_posts()): $loop->the_post(); if($i++<1){?>
<div class="main-list clearfix">
    <a href="<?php the_permalink();?>">
        <?php the_post_thumbnail('medium');?>
    </a>
    <h5 class="pt-2">
        <a href="<?php the_permalink();?>">
            <?php the_title();?>
        </a>
    </h5>
</div>

<hr>
<div class="custom-list">
    <?php } else{?>
    <div class="media custom-list-item">
        <i class="fas fa-square-full align-self-center"></i>
        <div class="media-body">
            <h6 class="mt-0 ml-3">
                <a href="<?php the_permalink();?>">
                    <?php the_title();?>
                </a>
            </h6>
        </div>
    </div>
   <?php } endwhile; wp_reset_postdata();?>
  </div>
  </div>