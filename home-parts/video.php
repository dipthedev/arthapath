
    <div class="cat-title text-center"> भिडियो
    </div>
   <div class="video-background  h-100" style=" background: #000000;">
        <div class="owl-carousel slides">
            <?php $i=0; $recent = new WP_Query(array('cat' => '27', 'showposts' => 21));
            while ($recent->have_posts()) : $recent->the_post();?>
            <?php if( get_field('video_link') ): ?>
            <div class="item-video" data-hash="<?php echo $i++;?>"><a class="owl-video"
                    href="<?php the_field('video_link');?>"></a>
            </div>
            <?php endif; ?>
            <?php endwhile;wp_reset_postdata();?>
        </div>
    </div>
<style>
    .video .cat-title {
        border-bottom: solid 1px #ddd;
        font-size: 20px;
    }

    .video .cat-title h3 {
        color: #ddd;
        padding-top: 10px;
    }

    .video-list h5 {
        color: #fff;
    }

    .video-list .media {
        margin-bottom: 10px;
        -webkit-transition: 0.5s;
        -moz-transition: 0.5s;
        transition: 0.5s;
    }

    .video-list .media:hover,
    .video-list .media:focus {

        -webkit-transition: 0.5s;
        -moz-transition: 0.5s;
        transition: 0.5s;
    }

    .owl-video-wrapper {
        height: 250px !important;
    }

    .owl-carousel .owl-video-wrapper {
        position: relative;
        height: 100%;
    }
</style>

