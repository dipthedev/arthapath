<div class="cat-title">विचार</div>
<div class="mt-3" style="border: dotted 1px #bbbec3; padding: 10px;">
    <?php $args = array('showposts' =>5, 'cat' => '50'); $loop = new WP_Query( $args ); while($loop->have_posts()): $loop->the_post();?>
    <div class="media my-list mb-3">
        <a href="<?php the_permalink();?>">
            <div class="round">
                <?php the_post_thumbnail('thumbnail');?>
            </div>
        </a>
        <div class="media-body pl-3" style="border-bottom: dotted 1px #cbd0de;">
            <a href="<?php the_permalink();?>">
                <h5 class="mt-0" style="font-weight: 600; font-size:20px;">
                    <?php echo wp_trim_words( get_the_title(), 10 ); ?>
                </h5>
            </a>
            </div>
    </div>
    
        <?php  endwhile; wp_reset_postdata();?>
    </div>
