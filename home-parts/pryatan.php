<div class="cat-title">विश्व बजार</div>
<div class="mt-3" style="border: dotted 1px #bbbec3; padding: 10px;">
    <?php $args = array('showposts' =>4, 'cat' => '62'); $loop = new WP_Query( $args ); while($loop->have_posts()): $loop->the_post();?>
    <div class="media mb-3">
        <a href="<?php the_permalink();?>">
            <div class="max-fan mbl-thumb">
                <?php the_post_thumbnail('thumbnail');?>
            </div>
        </a>
        <div class="media-body pl-3">
            <a href="<?php the_permalink();?>">
                <h5 class="mt-0" style="font-weight: 600; font-size:20px;">
                    <?php echo wp_trim_words( get_the_title(), 10 ); ?>
                </h5>
                <p  class="mbl-pp" style="font-size: 20px;">
                <?php echo wp_trim_words(get_the_excerpt(),10,'');?>
            </p>
            </a>
            </div>
    </div>
    
        <?php  endwhile; wp_reset_postdata();?>
    </div>
