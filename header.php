<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="wikinhow is a how to  blog  providing how-to content , when to content , tips  and technology news &amp; more about lifestyle, tips ,health, &amp; wikinhow" />
    <meta property="og:description" content="wikinhow is a trusted blog providing you a how-to content of everything.Search for how-to ,tech related news,health related news,tips and life hacks" />
    <meta property="og:title" content="How-to content of all Latest Technology News,tips about life,life style,wikinhow" />
    <title>
        <?=wp_title('&laquo;',true,'right')?>
        <?= bloginfo('name')?>
    </title>
    <?php wp_head();?>
    <!-- Google Analytics -->
    <script>
    window.ga = window.ga || function() {
        (ga.q = ga.q || []).push(arguments)
    };
    ga.l = +new Date;
    ga('create', 'UA-103301821-1', 'auto');
    ga('send', 'pageview');
    </script>
    <script async src='https://www.google-analytics.com/analytics.js'></script>
    <!-- End Google Analytics -->

</head>

<body id="main-body">
    <div id="fb-root"></div>
    <script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=401514826619301";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>
    <div class="pt-0" style="background-color:#ffffff!important;">
    <div class="bg-red" style="background: whitesmoke;">
    <?php get_template_part('home-parts/top-nav');?>
    </div>
   <div class="container"> <div class="row mb-4">
   <div class="col-md-4">
   <?php dynamic_sidebar('logoadd1');?>
</div>
   <div class="col-md-4">
   <div class="position pt-3 no-pad" style="left: -64px;">
    <a href="/"> <img src="<?php   echo get_stylesheet_directory_uri();?>/img/logo.png" class="logo pt-3 pb-3" alt="Logo" id="nav-logo"> </a>
<span class="date_time my-date">
  <?php echo the_date();?></span>
   </div>
   </div>
   <div class="col-md-4 mt-5">
   <?php dynamic_sidebar('logoadd2');?>
   <div class="khabarhub-search pt-3 mbl-padding">
       <form class="searchbar" action="/" method="get">
            <input class="search_input" type="search" name="s" placeholder="यहाँ खोज्नुहोस ..." value="<?php the_search_query();?>">
            <button type="submit" class="search_icon">
                <i class="fas fa-search"></i>
            </button>
        </form>
    </div>
   </div>
   </div> 
    </div>
</div>
 <div class=" pt-0 sticky-top">
        <?php get_template_part('nav');?>
        </div>
       
    
   