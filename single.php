<?php get_header();?>
<div class="container">
    <?php while ( have_posts() ) : the_post();?>
    <div class="row single">
        <?php get_template_part('template-parts/content',get_post_format());?>
    </div>
    <!-- .row -->
    <?php endwhile;?>
</div>
<?php get_footer();?>