<?php get_header();?>
<div class="container">
<div class="mt-5">
<?php dynamic_sidebar('banneeradd-1');?>
</div>
    <div class=" mt-4">
        <?php get_template_part('home-parts/banner');?>
    </div>
    <div class="mt-5">
<?php dynamic_sidebar('banneeradd-2');?>
</div>
   <div class=" row mt-5">
  <div class="col-md-9">
    <?php get_template_part('home-parts/flash');?>
    </div>
  <div class="col-md-3" style="background: #f7f7fb;">
    <?php get_template_part('home-parts/');?>
    </div>
</div>
<div class="mt-5">
<?php dynamic_sidebar('banneeradd-3');?>
</div>
  <div class="row  mt-4">
        <div class="col-md-8">
            <?php get_template_part('home-parts/main');?>
        </div>
        <div class="col-md-4 mt-5" style="background: #f7f7fb;">
            <div class="mt-3">
        <?php dynamic_sidebar('sidebaradd-1');?>
        </div>
        </div>
    </div>
    <div class="mt-5">
<?php dynamic_sidebar('banneeradd-4');?>
</div>
<div class="  row mt-3">
      <div class="col-md-6">
  <?php get_template_part('home-parts/share');?>
  </div>
      <div class="col-md-6">
  <?php get_template_part('home-parts/bima');?>
  </div>
        </div>
        <div class=" mb-5">
         <?php get_template_part('home-parts/interview');?>
        </div>
  <div class=" mb-5 row">
      <div class="col-md-8">
  <?php get_template_part('home-parts/politics');?>
  </div>
      <div class="col-md-4">
      <?php get_template_part('home-parts/bichar');?>
  </div>
        </div>
  <div class=" mb-5 row">
      <div class="col-md-4">
  <?php get_template_part('home-parts/prabidhi');?>
  </div>
      <div class="col-md-4">
      <?php get_template_part('home-parts/corporate');?>
  </div>
  <?php if ( is_active_sidebar( 'sidebaradd-1') ) { ?>
    <div class="col-md-4 mt-5">
    <?php dynamic_sidebar('sidebaradd-1');?></div>
  <?php } else{?>
    <div class="col-md-4 mt-5" style="background: #f7f7fb;">
     
</div>
  <?php }?>
     
        </div>
        <div class="row">
        <div class="col-md-4">
        <?php get_template_part('home-parts/movements');?>
        </div>
         <div class="col-md-4">
        <?php get_template_part('home-parts/krisi');?>
        </div>
        <div class="col-md-4">
            <?php get_template_part('home-parts/rojgar');?>
        </div>
        </div>
    <div class="row">
        <div class="col-md-12">
            <?php get_template_part('home-parts/video');?>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-md-5">
            <?php get_template_part('home-parts/pryatan');?>
        </div>
     
    <div class="col-md-4">
            <?php get_template_part('home-parts/samaj');?>
        </div>
    </div>
    <div class=" mt-5 row">
    <div class="col-md-9">
    <?php get_template_part('home-parts/purbadhar');?>
    </div>
    <div class="col-md-3"></div>
       </div>
  
</div>
<!-- .home -->
<?php get_footer();?>