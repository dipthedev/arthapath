<?php $i=0; while ( have_posts() ) : the_post();if($i++<1){?>
<div class="mt-3 international-img">
        <a href="<?php the_permalink();?>">
           <div class="back-img loop-img"
                style="background: url(<?php echo get_the_post_thumbnail_url( $post->ID,'full' ); ?>)center 0px / cover no-repeat;height:500px;">
            </div></a>
            <div class="overlay-information"></div>
           <h5 class="pt-3 international-text text-light" style="font-size:30px;">
                 <a href="<?php the_permalink();?>"  class="text-dark"> <?php the_title();?></a>
                </h5>
                <p class="lead p-3">
                <?php echo wp_trim_words(get_the_excerpt(),50,'');?>
            </p>
</div>
<hr>
<div class="row mt-3">
<?php } else{ ?>
    <div class="col-md-6">
    <div class="media loksewa-list pt-3 pb-3">
             <a href="<?php the_permalink();?>">
             <div class="mr-3 sr-thumb-t">
                     <?php the_post_thumbnail('thumbnail');?>
                 </div>
             </a>
             <div class="media-body">
            
                     <h5 class="mt-0" style="font-weight: 600; font-size: 20px;">
                          <a href="<?php the_permalink();?>"> <?php the_title();?> </a>
                     </h5>
                  </div>
         </div> 
         </div>
    <?php } endwhile;appharu_paging();?>
    </div>