<?php get_header();?>
<?php /* Template Name:Our  Team */ ?>
<div class="container mt-3 mb-3 ">
    <div class="contact">
      
        <div class="row">
            <div class="col-md-2">
                <img class="footlogo" src="<?php echo get_stylesheet_directory_uri();?>/img/logo.png" alt="Logo">
            </div>
            <div class="col-md-6 mt-2">
            <h1 class="d-block"> <span style="border-bottom:solid 5px white">हाम्रो टिम</span></h1>
            <?php while ( have_posts() ) : the_post();?>
            <article class="post-entry">
        <?php the_content();?> </article>
    </div>
   </div>
</div>
<?php endwhile;?>
            </div>
            <!-- <div class="col-md-4">
            <div class="fb-page" data-href="https://www.facebook.com/arthapath/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/arthapath/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/arthapath/">Post Khabar</a></blockquote></div>

            </div> -->
        </div>
    </div>
</div>
<?php get_footer();?>