<footer class="mt-4">
    <div class="container pb-4">
    <div class="row">
            <div class="col-md-6 pt-4">
                <div class="row">
                    <div class="col-md-6 pt-3">
                        <div class="brand-log  mbl-excerpt "><a href="/"> <img
                                    src="<?php  echo get_stylesheet_directory_uri();?>/img/logo.png" class="logo"
                                    alt="Logo" id="nav-logo"> </a>
                                  
                                </div>
                        
                    </div>
                    <div class="col-md-6">
                    <h5 class="foot-degi text-light text-left border-b">हाम्रो बारे</h5>
                        <h5 class="foot-degi-s text-left text-light">एक्सप्रेस त्रिशुली मिडिया प्रा. लि.</h5>
                        <h5 class="foot-title"> <i class="fas fa-map-marker-alt"></i>  बागबजार, काठमाडौं</h5>
                        <h5 class="foot-title"><i class="fa fa-phone"></i> +९७७-०१४१६७५९१</h5>
                        <h5 class="foot-title"><i class="fas fa-envelope-square"></i> arthapath@gmail.com</h5>
                        <h5 class="foot-title"><i class="fas fa-envelope-square"></i>info@arthapath.com</h5>
                        <h5 class="foot-title">दर्ता नं.: १७०८/०७६-७७</h5>
                        <h5 class="foot-title">www.arthapath.com</h5>

                    </div>
                </div>
            </div>
            <div class="col-md-6 pt-4">
                <div class="row mbl-border ">
                <div class="col-md-6">
                    <h5 class="foot-degi text-light text-left border-b">हाम्रो टिम</h5>
                    <h5 class="foot-degi-s text-left text-light font-s"><i class="fas fa-angle-right"></i>&nbsp;&nbsp;Editor in Chief:  <span class="p-name">Bishnu Taruke</span> </h5>
                    <h5 class="team-foot" style="color: #e8eeff;font-size: 17px;">All Team &nbsp; <span class="ico-foot"><i class="fas fa-arrow-right"></i></span> </h5>
                    <ul class="socio-bar pt-3">
                            <a href="https://www.facebook.com/arthapath/"><li><i class="fab fa-facebook-f"></i></li></a>
                            <a href="https://twitter.com/"><li><i class="fab fa-twitter"></i></li></a>
                            <a href="https://www.youtube.com/watch?v=I1GP3MLybJE"> <li><i class="fab fa-youtube"></i></li> </a>
                            <!-- <li><i class="fab fa-instagram"></i></li> -->
                        </ul>
                </div>
                    <div class="col--md-6 text-center pt-4">
                        <h5 class="foot-degi text-center text-light">महत्वपूर्ण लिङ्कहरू</h5>
                        <div class="d-flex">
                            <div class="col-6">
                                <ul>
                                <li class="name">गृह पृष्ठ</li>
                                <li class="name">हाम्रो बारेमा</li>
                                <li class="name">युनिकोड</li>
                                </ul>
                            </div>
                            <div class="col-6">
                                <ul>
                                <li class="name">बिज्ञापन</li>
                                <li class="name">ई-पेपर</li>
                                <li class="name">सम्पर्क</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</footer>
<div class="copyright" style="background:#0a2546e0;">
    <div class="container">
        <div class="d-flex justify-content-between mbl-flex">
            <div class="g">
                <a class=" text-left" style="color:white;font-size: 16px;">&copy;
                    <?php echo date('Y').' '; bloginfo( 'name' );?>


                </a></div>
            <div class=" applink mbl-center">
                <a href="http://www.appharu.com?/ref=arthapath" class="developer-link text-light"
                    style="color: white!important;font-size: 18px;">By : Appharu</a>
                </span>
            </div>
        </div>
    </div>
    <?php wp_footer();?>
  <!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f2a59f850c60dd3"></script>


    </body>
</html>
